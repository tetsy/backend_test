// import testPeoplesClassic from './data/data_classic.json';

const {filterAnimals, addChildNumberToNodes} = require('../src/services/people');
const testPeoplesClassic = require('./data/data_classic.json');
const testPeoplesEmpty = require('./data/data_empty.json');
const testPeoplesRandom = require('./data/data_random.json');
const testPeoplesOther = require('./data/data_other.json');
const testPeoplesTest = require('./data/data_test.json');


describe('filterAnimals', () => {
    test('should return an empty array if there is no animal with the pattern [classic]', () => {
      const filteredAnimals = filterAnimals(testPeoplesClassic, 'lion');
      expect(filteredAnimals).toEqual([]);
    }); 

    test('should return an empty array if there is no animal with the pattern [empty]', () => {
        const filteredAnimals = filterAnimals(testPeoplesEmpty, 'lion');
        expect(filteredAnimals).toEqual([]);
      });

    test('should return an empty array if there is no animal with the pattern [other]', () => {
        const filteredAnimals = filterAnimals(testPeoplesOther, 'lion');
        expect(filteredAnimals).toEqual([]);
      }); 
    
    test('should filter animals with the pattern  [1][classic]', () => {
      const filteredAnimals = filterAnimals(testPeoplesClassic, 'Dory');
      expect(filteredAnimals).toEqual([
        {
          "name": "Uzuzozne",
          "people": [
            {
              "name": "Lillie Abbott",
              "animals": [
                {
                  "name": "John Dory"
                }
              ]
            }
          ]
        }
      ]);
    });

    test('should filter animals with the pattern [2][classic]', () => {
        const filteredAnimals = filterAnimals(testPeoplesClassic, 'ry');
        expect(filteredAnimals).toEqual([
            {
              "name": "Uzuzozne",
              "people": [
                {
                  "name": "Lillie Abbott",
                  "animals": [
                    {
                      "name": "John Dory"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Satanwi",
              "people": [
                {
                  "name": "Anthony Bruno",
                  "animals": [
                    {
                      "name": "Oryx"
                    }
                  ]
                }
              ]
            }
          ]);
      });

    test('should filter no animals with the empty pattern [classic]', () => {
        const filteredAnimals = filterAnimals(testPeoplesClassic, '');
        expect(filteredAnimals).toEqual([
            {
              "name": "Uzuzozne",
              "people": [
                {
                  "name": "Lillie Abbott",
                  "animals": [
                    {
                      "name": "John Dory"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Satanwi",
              "people": [
                {
                  "name": "Anthony Bruno",
                  "animals": [
                    {
                      "name": "Oryx"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Dillauti",
              "people": [
                {
                  "name": "Winifred Graham",
                  "animals": [
                    {
                      "name": "Anoa"
                    },
                    {
                      "name": "Duck"
                    },
                    {
                      "name": "Narwhal"
                    },
                    {
                      "name": "Badger"
                    },
                    {
                      "name": "Cobra"
                    },
                    {
                      "name": "Crow"
                    }
                  ]
                },
                {
                  "name": "Blanche Viciani",
                  "animals": [
                    {
                      "name": "Barbet"
                    },
                    {
                      "name": "Rhea"
                    },
                    {
                      "name": "Snakes"
                    },
                    {
                      "name": "Antelope"
                    },
                    {
                      "name": "Echidna"
                    },
                    {
                      "name": "Crow"
                    },
                    {
                      "name": "Guinea Fowl"
                    },
                    {
                      "name": "Deer Mouse"
                    }
                  ]
                }
              ]
            }
          ]);
      });

      test('should return an empty array if there is no animal with the pattern [1][random]', () => {
        const filteredAnimals = filterAnimals(testPeoplesRandom, 'oé"iè(ç_é');
        expect(filteredAnimals).toEqual([]);
      }); 

      test('should return an empty array if there is no animal with the type number [2][random]', () => {
        const filteredAnimals = filterAnimals(testPeoplesRandom, 675879);
        expect(filteredAnimals).toEqual([]);
      });

      test('should return an empty array if there is no animal with the type array [3][random]', () => {
        const filteredAnimals = filterAnimals(testPeoplesRandom, ["likldfjon"]);
        expect(filteredAnimals).toEqual([]);
      });

      test('should return an empty array if there is no animal with the type object [4][random]', () => {
        const filteredAnimals = filterAnimals(testPeoplesRandom, {"sdqfhosqdf":"hu"});
        expect(filteredAnimals).toEqual([]);
      });

      test('should return an array without the incorrectly formatted objects', () => {
        const filteredAnimals = filterAnimals(testPeoplesTest, "");
        expect(filteredAnimals).toEqual([
            {
              "name": "Uzuzozne",
              "people": [
                {
                  "name": "Lillie Abbott",
                  "animals": [
                    {
                      "name": "John Dory"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Dillauti",
              "people": [
                {
                  "name": "Winifred Graham",
                  "animals": [
                    {
                      "name": "Narwhal"
                    },
                    {
                      "name": "Barbet"
                    }
                  ]
                }
              ]
            }
          ]);
      });
});
  
describe('addChildNumberToNodes', () => {

    test('should return an empty array [empty]', () => {
        const numbersPeoples = addChildNumberToNodes(testPeoplesEmpty);
        expect(numbersPeoples).toEqual([]);
    });

    test('should return an empty array [empty]', () => {
        const numbersPeoples = addChildNumberToNodes(testPeoplesEmpty);
        expect(numbersPeoples).toEqual([]);
    });

    test('should return a table with the numbers of animals and people [1][classic]', () => {
        const numbersPeoples = addChildNumberToNodes(testPeoplesClassic);
        expect(numbersPeoples).toEqual([
            {
              "name": "Uzuzozne [2]",
              "people": [
                {
                  "name": "Lillie Abbott [1]",
                  "animals": [
                    {
                      "name": "John Dory"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Satanwi [2]",
              "people": [
                {
                  "name": "Anthony Bruno [1]",
                  "animals": [
                    {
                      "name": "Oryx"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Dillauti [16]",
              "people": [
                {
                  "name": "Winifred Graham [6]",
                  "animals": [
                    {
                      "name": "Anoa"
                    },
                    {
                      "name": "Duck"
                    },
                    {
                      "name": "Narwhal"
                    },
                    {
                      "name": "Badger"
                    },
                    {
                      "name": "Cobra"
                    },
                    {
                      "name": "Crow"
                    }
                  ]
                },
                {
                  "name": "Blanche Viciani [8]",
                  "animals": [
                    {
                      "name": "Barbet"
                    },
                    {
                      "name": "Rhea"
                    },
                    {
                      "name": "Snakes"
                    },
                    {
                      "name": "Antelope"
                    },
                    {
                      "name": "Echidna"
                    },
                    {
                      "name": "Crow"
                    },
                    {
                      "name": "Guinea Fowl"
                    },
                    {
                      "name": "Deer Mouse"
                    }
                  ]
                }
              ]
            }
          ]);
    });

    test('should return a table with the numbers of animals and people [1][test]', () => {
        const numbersPeoples = addChildNumberToNodes(testPeoplesTest);
        expect(numbersPeoples).toEqual([
            {
              "name": "Uzuzozne [2]",
              "people": [
                {
                  "name": "Lillie Abbott [1]",
                  "animals": [
                    {
                      "name": "John Dory"
                    }
                  ]
                }
              ]
            },
            {
              "name": "Dillauti [7]",
              "people": [
                {
                  "name": "Winifred Graham [3]",
                  "animals": [
                    {
                      "namsvsde": "Duck"
                    },
                    {
                      "name": "Narwhal"
                    },
                    {
                      "name": "Barbet"
                    }
                  ]
                },
                {
                  "animalswfxf": [
                    {
                      "name": "Barbet"
                    },
                    {
                      "name": "Barbket"
                    },
                    {
                      "name": "Rhea"
                    }
                  ]
                },
                {
                  "name": "Russia",
                  "people": [
                    {
                      "name": "Shani McCray",
                      "animals": [
                        {
                          "namea": "Whale, southern right"
                        },
                        {
                          "name": "Brush-tailed rat kangaroo"
                        }
                      ]
                    }
                  ]
                },
                {
                  "name": "Indonesia",
                  "people": [
                    {
                      "name": "Randa Search"
                    }
                  ]
                }
              ]
            }
          ]);
    });


}); 