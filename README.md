# Application to test my backend skills in TypeScript

The objective of this application can be found in the file BACKEND_TEST.md

## Installation via Docker
Building the `backend_test` application via Docker

```shell script
$ git clone https://gitlab.com/tetsy/backend_test.git
...
$ cd backend_test
...
$ docker build -t backend_test .
...
```
## Use via Docker

### Run the tests

```shell script
$ docker run backend_test npm test
> test
> jest

PASS tests/people.test.ts
  filterAnimals
    ✓ should return an empty array if there is no animal with the pattern [classic] (3 ms)
    ✓ should return an empty array if there is no animal with the pattern [empty]
    ✓ should return an empty array if there is no animal with the pattern [other] (1 ms)
    ✓ should filter animals with the pattern  [1][classic] (1 ms)
    ✓ should filter animals with the pattern [2][classic] (1 ms)
    ✓ should filter no animals with the empty pattern [classic]
    ✓ should return an empty array if there is no animal with the pattern [1][random] (1 ms)
    ✓ should return an empty array if there is no animal with the type number [2][random] (1 ms)
    ✓ should return an empty array if there is no animal with the type array [3][random] (1 ms)
    ✓ should return an empty array if there is no animal with the type object [4][random] (1 ms)
    ✓ should return an array without the incorrectly formatted objects (1 ms)
  addChildNumberToNodes
    ✓ should return an empty array [empty]
    ✓ should return an empty array [empty]
    ✓ should return a table with the numbers of animals and people [1][classic] (1 ms)
    ✓ should return a table with the numbers of animals and people [1][test] (1 ms)

Test Suites: 1 passed, 1 total
Tests:       15 passed, 15 total
Snapshots:   0 total
Time:        0.585 s
Ran all test suites.

```

### Run the application

```shell script
$ docker run backend_test node src/app.js --help
#####  Help for using the program #####
Argument  : Description of the argument

--help    : Argument to display the help

--filter  : Argument for filtering the data 
Type      : string
Default   : None

--count   : Argument to display the data with the number of nodes (can be combined with --filter)

--version : Argument to know the version of the program

Example: node app.js --filter=test

```