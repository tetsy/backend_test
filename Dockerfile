FROM node:18-alpine
WORKDIR /app
COPY . .
RUN npm install jest --global
RUN npm install --production
# CMD ["node"]