"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
// import peoples from '../data/data.json';
var data_classic_json_1 = __importDefault(require("../tests/data/data_classic.json"));
var people_1 = require("./services/people");
var utils_1 = require("./utils/utils");
var constants_1 = require("./constants");
/**
 * @description Displays the program help in the console
 */
function help() {
    console.log("#####  Help for using the program #####");
    console.log("Argument  : Description of the argument");
    console.log("");
    console.log("--help    : Argument to display the help");
    console.log("");
    console.log("--filter  : Argument for filtering the data ");
    console.log("Type      : string");
    console.log("Default   : None");
    console.log("");
    console.log("--count   : Argument to display the data with the number of nodes (can be combined with --filter)");
    console.log("");
    console.log("--version : Argument to know the version of the program");
    console.log("");
    console.log("Example: node app.js --filter=test");
}
/**
 * @description Displays the version of the program in the console
 */
function version() {
    console.log("Version ".concat(constants_1.VERSION));
}
/**
 * @descrition Checks, formats and returns program arguments
 * @returns Returns an object containing the values and arguments of the program
 */
function getFormattedArguments() {
    var argsObject = {};
    process.argv.forEach(function (value, index) {
        if (index > 1 && value) {
            var args = value.split('=');
            var arg = args[0].trim().toLowerCase();
            var argValue = args[1];
            if (!constants_1.ARGUMENTS.includes(arg)) {
                console.log("The argument \"".concat(args[0], "\" does not exist."));
                return {};
            }
            else {
                argsObject[arg] = argValue;
            }
        }
    });
    return argsObject;
}
var args = getFormattedArguments();
var argsList = Object.keys(args);
if (argsList.length === 0 || argsList.includes('--help')) {
    help();
    process.exit(0);
}
if (argsList.includes('--version')) {
    version();
    process.exit(0);
}
var peoplesData = data_classic_json_1["default"];
if (argsList.includes('--filter')) {
    peoplesData = (0, people_1.filterAnimals)(peoplesData, args['--filter']);
}
if (argsList.includes('--count')) {
    peoplesData = (0, people_1.addChildNumberToNodes)(peoplesData);
}
(0, utils_1.printData)(peoplesData);
process.exit(0);
