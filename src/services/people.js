"use strict";
exports.__esModule = true;
exports.addChildNumberToNodes = exports.filterAnimals = void 0;
var utils_1 = require("../utils/utils");
/**
 * @param peoples
 * @param pattern
 * @returns Returns a list of objects filtered on the name of the animal
 */
function filterAnimals(peoples, pattern) {
    var dataPeoples = (0, utils_1.clone)(peoples);
    var filteredPeoples = dataPeoples.map(function (people) {
        if (people.hasOwnProperty('people')) { // check key
            var filteredPerson = people.people.map(function (person) {
                if (person.hasOwnProperty('animals')) {
                    var filteredAnimals = person.animals.filter(function (animal) {
                        if (animal.hasOwnProperty('name')) {
                            return animal.name.includes(pattern);
                        }
                        return false;
                    });
                    person.animals = (0, utils_1.cleanArray)(filteredAnimals);
                    return person.animals.length > 0 ? person : null;
                }
                return null;
            });
            people.people = (0, utils_1.cleanArray)(filteredPerson);
            return people.people.length > 0 ? people : null;
        }
        return null;
    });
    return (0, utils_1.cleanArray)(filteredPeoples);
}
exports.filterAnimals = filterAnimals;
/**
 *
 * @param peoples
 * @returns Returns a list of objects by adding the number of nodes to the node names
 */
function addChildNumberToNodes(peoples) {
    var dataPeoples = (0, utils_1.clone)(peoples);
    for (var i = 0; i < dataPeoples.length; i++) {
        var numbers = [];
        if (dataPeoples[i].hasOwnProperty('people')) {
            numbers.push(dataPeoples[i].people.length);
            for (var j = 0; j < dataPeoples[i].people.length; j++) {
                if (dataPeoples[i].people[j].hasOwnProperty('animals')) {
                    var numberAnimals = dataPeoples[i].people[j].animals.length;
                    numbers.push(numberAnimals);
                    if (dataPeoples[i].people[j].hasOwnProperty('name')) {
                        dataPeoples[i].people[j].name = dataPeoples[i].people[j].name + " [".concat(numberAnimals, "]");
                    }
                }
            }
        }
        var total = numbers.reduce(function (partialSum, a) { return partialSum + a; }, 0);
        if (dataPeoples[i].hasOwnProperty('name')) {
            dataPeoples[i].name = dataPeoples[i].name + " [".concat(total, "]");
        }
    }
    return dataPeoples;
}
exports.addChildNumberToNodes = addChildNumberToNodes;
