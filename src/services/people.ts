import {cleanArray, clone} from '../utils/utils';
/**
 * @param peoples 
 * @param pattern 
 * @returns Returns a list of objects filtered on the name of the animal
 */
export function filterAnimals(peoples: any, pattern: string): any {
    var dataPeoples = clone(peoples);
    var filteredPeoples = dataPeoples.map((people) => {
        if (people.hasOwnProperty('people')){ // check key
            var filteredPerson = people.people.map((person) => {
                if (person.hasOwnProperty('animals')){ 
                    var filteredAnimals = person.animals.filter((animal) => {
                        if (animal.hasOwnProperty('name')){
                            return animal.name.includes(pattern);
                        }
                        return false;
                    });
                    
                    person.animals = cleanArray(filteredAnimals);
        
                    return person.animals.length > 0 ? person : null;
                }
                return null;
            });
    
            people.people = cleanArray(filteredPerson);
            
            return people.people.length > 0 ? people : null;
        }

        return null;
    });

    return cleanArray(filteredPeoples);
}

/**
 * 
 * @param peoples 
 * @returns Returns a list of objects by adding the number of nodes to the node names
 */
export function addChildNumberToNodes(peoples: any): any {
    var dataPeoples = clone(peoples);
    for (let i = 0; i < dataPeoples.length; i++) {
        var numbers: number[] = [];
        
        if (dataPeoples[i].hasOwnProperty('people')){ 
            numbers.push(dataPeoples[i].people.length);
            
            for (let j = 0; j < dataPeoples[i].people.length; j++) {
                if (dataPeoples[i].people[j].hasOwnProperty('animals')){ 
                    let numberAnimals: number = dataPeoples[i].people[j].animals.length;
                    numbers.push(numberAnimals);
                    if (dataPeoples[i].people[j].hasOwnProperty('name')){  
                        dataPeoples[i].people[j].name = dataPeoples[i].people[j].name +` [${numberAnimals}]`;
                    }
                }
            }
        }
        
        let total: number = numbers.reduce((partialSum, a) => partialSum + a, 0);
        
        if (dataPeoples[i].hasOwnProperty('name')){ 
            dataPeoples[i].name = dataPeoples[i].name + ` [${total}]`; 
        }
    } 

    return dataPeoples;
}