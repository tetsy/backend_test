export const VERSION = '1.0.0';
export const ARGUMENTS = [
    '--filter',
    '--count',
    '--help',
    '--version'
];