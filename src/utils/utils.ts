/**
 * @description Display a formatted object in the console
 * @param data 
 */
export function printData(data: object): void{
    console.log(JSON.stringify(data, null, "  "));
}

/**
 * 
 * @param dataArray 
 * @returns Returns an array cleaned of any empty values
 */
export function cleanArray(dataArray){
    return dataArray.filter(function(value) {
        return value !== undefined && value !== null && value !== "" && value.length !== 0;
    });
}

/**
 * 
 * @param data 
 * @returns Returns a cloned object
 */
export function clone(data: any){
    return JSON.parse(JSON.stringify(data));
}