"use strict";
exports.__esModule = true;
exports.clone = exports.cleanArray = exports.printData = void 0;
/**
 * @description Display a formatted object in the console
 * @param data
 */
function printData(data) {
    console.log(JSON.stringify(data, null, "  "));
}
exports.printData = printData;
/**
 *
 * @param dataArray
 * @returns Returns an array cleaned of any empty values
 */
function cleanArray(dataArray) {
    return dataArray.filter(function (value) {
        return value !== undefined && value !== null && value !== "" && value.length !== 0;
    });
}
exports.cleanArray = cleanArray;
/**
 *
 * @param data
 * @returns Returns a cloned object
 */
function clone(data) {
    return JSON.parse(JSON.stringify(data));
}
exports.clone = clone;
