// import peoples from '../data/data.json';
import peoples from '../tests/data/data_classic.json';
import {filterAnimals, addChildNumberToNodes} from './services/people';
import {printData} from './utils/utils';
import { ARGUMENTS, VERSION } from './constants';


/**
 * @description Displays the program help in the console
 */
function help(): void {
    console.log("#####  Help for using the program #####");
    console.log("Argument  : Description of the argument");
    console.log("");
    console.log("--help    : Argument to display the help");
    console.log("");
    console.log("--filter  : Argument for filtering the data ");
    console.log("Type      : string");
    console.log("Default   : None");
    console.log("");
    console.log("--count   : Argument to display the data with the number of nodes (can be combined with --filter)");
    console.log("");
    console.log("--version : Argument to know the version of the program");
    console.log("");
    console.log("Example: node app.js --filter=test");
}

/**
 * @description Displays the version of the program in the console
 */
function version(): void {
    console.log(`Version ${VERSION}`);
}

/**
 * @descrition Checks, formats and returns program arguments 
 * @returns Returns an object containing the values and arguments of the program 
 */
function getFormattedArguments(): object {
    var argsObject:object = {};

    process.argv.forEach((value, index) => {
        if(index > 1 && value){
            var args:string[] = value.split('=');
            var arg:string = args[0].trim().toLowerCase();
            var argValue:string = args[1];
            
            if (!ARGUMENTS.includes(arg)){
                console.log(`The argument "${args[0]}" does not exist.`);
                return {};
            }
            else{
                argsObject[arg] = argValue;
            }
        }
    });

    return argsObject;
}

var args:object = getFormattedArguments();

var argsList:string[] = Object.keys(args);

// If no argument or the --help argument is passed then the help is displayed 
if (argsList.length === 0 || argsList.includes('--help') ){
    help();
    process.exit(0);
}

// Displays the program version if --version
if (argsList.includes('--version') ){
    version();
    process.exit(0);
}

var peoplesData = peoples;

// Important, if there are two arguments --filter and --count then --filter must be executed first
if (argsList.includes('--filter')){
    peoplesData = filterAnimals(peoplesData, args['--filter']);
}

if (argsList.includes('--count')){
    peoplesData = addChildNumberToNodes(peoplesData);
}

// Displays data nicely
printData(peoplesData);

process.exit(0);



