"use strict";
exports.__esModule = true;
exports.ARGUMENTS = exports.VERSION = void 0;
exports.VERSION = '1.0.0';
exports.ARGUMENTS = [
    '--filter',
    '--count',
    '--help',
    '--version'
];
